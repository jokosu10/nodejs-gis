var express = require('express');
var router = express.Router();
var models = require('../models/index');

// get data poi
router.get('/data', (req, res) => {
  models.pois
    .findAll()
    .then(pois => res.json(pois))
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

// post data poi
router.post('/data', (request, response) => {
  var point = { type: 'Point', coordinates: [request.body.lat, request.body.lng], crs: { type: 'name', properties: { name: 'EPSG:4326' } }};
  models.pois.create({
    name: request.body.name,
    geom: point
    })
    .then(function (pois) {
      return response.json(pois);
    })
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

// search name data poi by id
router.get('/data/:id', (request, response) => {
  const id = request.params.id;
  models.pois.find({ where: { id: id } })
    .then(function (pois) {
      if (pois == null) {
        return response.json('Not Found');
      }

      return response.json(pois);
    })
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

// mengubah pois yang ada
router.put('/data/:id', (request, response) => {
    const id = request.params.id;
    var selector = {
      where: { id: id }
    };
    models.pois.findOne({ where: { id: id } })
      .then(function (pois) {
        if (pois == null) {
          return response.json('Not Found');
        }
        var point = { type: 'Point', coordinates: [request.body.lat, request.body.lng], crs: { type: 'name', properties: { name: 'EPSG:4326' } } };
        // return models.pois.update({
        //   name: request.body.name || pois.name,
        //   geom: point || pois.geom
        // })
        var values = {
          name: request.body.name || pois.name,
          geom: point || pois.geom
        }
        models.pois.update(values, selector)
      })
      .then(function (pois) {
        return response.json(pois);
      })
      .catch(function (err) {
        console.log('Something happend');
        console.log(err);
      });
});

// search name data poi by id
router.delete('/data/:id', (request, response) => {
  const id = request.params.id;
  models.pois.destroy({ where: { id: id } })
    .then(function (pois) {
      return response.json('Data has been deleted');
    })
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

module.exports = router;
