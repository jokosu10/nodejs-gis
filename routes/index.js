var express = require('express');
var router = express.Router();

// PostgreSQL and PostGIS module and connection setup
const { Client, Query } = require('pg');

// Setup connection
var username = "joko";
var password = "postgres";
var host = "localhost:5432";
var database = "postgis_js";
var conString = "postgres://" + username + ":" + password + "@" + host + "/" + database;

// set query
var coffee_query = "SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.geom)::json As geometry, row_to_json((id, name)) As properties FROM coffee_shops As lg) As f) As fc";
var point_query = "SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.geom)::json As geometry, row_to_json((id, name, geom)) As properties FROM pois As lg) As f) As fc";
var polygon_query = "SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.geom)::json As geometry, row_to_json((id, name, geom, luas)) As properties FROM areas As lg) As f) As fc";



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// get data
router.get('/data', (req, res) => {
  var client = new Client(conString);
  client.connect();
  var query = client.query(new Query(coffee_query));

  query.on("row", function(row, result) {
    result.addRow(row);
  });

  query.on("end", function(result) {
    res.send(result.rows[0].row_to_json);
    res.end();
  });
});

// get visualisasi map
router.get('/map', (req, res) => {
  var client = new Client(conString);
  client.connect();

  var query = client.query(new Query(coffee_query));
  query.on("row", function(row, result) {
    result.addRow(row);
  });

  // parsing result to the map page
  query.on("end", function(result) {
    var data = result.rows[0].row_to_json;
    res.render('map', {
      title: "express API",
      jsonData: data,
    });
  });
});

// get visualisasi maps point
router.get('/map/point', (req, res) => {
  var client = new Client(conString);
  client.connect();

  var query = client.query(new Query(point_query));

  query.on("row", function (row, result) {
    result.addRow(row);
  });

  // parsing result to the map page
  query.on("end", function (result) {
    var data = result.rows[0].row_to_json;
    res.render('maps', {
      title: "express API",
      jsonData: data,
    });
  });
});

// get visualisasi maps point
router.get('/map/polygon', (req, res) => {
  var client = new Client(conString);
  client.connect();

  var query = client.query(new Query(polygon_query));

  query.on("row", function (row, result) {
    result.addRow(row);
  });

  // parsing result to the map page
  query.on("end", function (result) {
    var data = result.rows[0].row_to_json;
    res.render('maps', {
      title: "express API",
      jsonData: data,
    });
  });
});

// get filtered page
router.get('/filter*', (req, res) => {
  var name = req.query.name;
  if (name.indexOf("--") > -1 || name.indexOf("'") > -1 || name.indexOf("/*") > -1 || name.indexOf("xp_") > -1) {
    console.log("bad request detected");
    res.redirect('/map');
    return;
  } else {
    console.log("request passed");
    var filter_query = "SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.geom)::json As geometry, row_to_json((id, name)) As properties FROM coffee_shops As lg WHERE lg.name = \'" + name + "\') As f) As fc";
    var client = new Client(conString);
    client.connect();
    var query = client.query(new Query(filter_query));
    query.on("row", function(row, result) {
      result.addRow(row);
    });
    query.on("end", function(result) {
      var data = result.rows[0].row_to_json;
      res.render('map', {
        title: "Express API",
        jsonData: data,
      });
    });
  }
});

module.exports = router;
