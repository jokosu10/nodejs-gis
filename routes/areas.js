var express = require('express');
var router = express.Router();
var models = require('../models/index');

// get data areas
router.get('/data', (req, res) => {
  models.areas
    .findAll()
    .then(areas => res.json(areas))
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

// post data areas
router.post('/data', (request, response) => {
  var polygon =
  {
    type: 'Polygon',
    coordinates: [
      [
        [request.body.pos1x, request.body.pos1y],
        [request.body.pos2x, request.body.pos2y],
        [request.body.pos3x, request.body.pos3y],
        [request.body.pos4x, request.body.pos4y],
        [request.body.pos5x, request.body.pos5y],
      ]
    ],
    crs: { type: 'name', properties: { name: 'EPSG:4326' } }
  };

  models.areas.create({
    name: request.body.name,
    geom: polygon,
    luas: request.body.luas,
    })
    .then(function (areas) {
      return response.json(areas);
    })
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

// search name data areas by id
router.get('/data/:id', (request, response) => {
  const id = request.params.id;
  models.areas.find({ where: { id: id } })
    .then(function (areas) {
      if (areas == null) {
        return response.json('Not Found');
      }

      return response.json(areas);
    })
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

// mengubah areas yang ada
router.put('/data/:id', (request, response) => {
    const id = request.params.id;
    var selector = {
      where: { id: id }
    };
    models.areas.findOne({ where: { id: id } })
      .then(function (areas) {
        if (areas == null) {
          return response.json('Not Found');
        }
        var polygon =
        {
          type: 'Polygon',
          coordinates: [
            [
              [request.body.pos1x, request.body.pos1y],
              [request.body.pos2x, request.body.pos2y],
              [request.body.pos3x, request.body.pos3y],
              [request.body.pos4x, request.body.pos4y],
              [request.body.pos5x, request.body.pos5y],
            ]
          ],
          crs: { type: 'name', properties: { name: 'EPSG:4326' } }
        };

        var values = {
          name: request.body.name || areas.name,
          geom: polygon || areas.geom,
          luas: request.body.luas || areas.luas,
        }
        models.areas.update(values, selector)
      })
      .then(function (areas) {
        return response.json(areas);
      })
      .catch(function (err) {
        console.log('Something happend');
        console.log(err);
      });
});

// search name data poi by id
router.delete('/data/:id', (request, response) => {
  const id = request.params.id;
  models.areas.destroy({ where: { id: id } })
    .then(function () {
      return response.json('Data has been deleted');
    })
    .catch(function (err) {
      console.log('Something happend');
      console.log(err);
    });
});

module.exports = router;
