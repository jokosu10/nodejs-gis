module.exports = {
  // eslint-disable-next-line
  up: async (queryInterface, Sequelize) => await queryInterface.createTable('pois', {
    'id': {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING,
      defaultValue: '',
      allowNull: false
    },
    geom: {
      type: Sequelize.GEOMETRY('POINT', 4326),
      defaultValue: '',
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: false,
      defaultValue: Sequelize.fn('NOW')
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: false,
      defaultValue: Sequelize.fn('NOW')
    }
  },
  {
    hooks: {
      beforeSave: (instance, options) => {
        console.log('Saving geom: ' + instance.geom);
        if (instance.geom && !instance.geom.crs) {
          instance.geom.crs = {
            type: 'name',
            properties: {
              name: 'EPSG:4326'
            }
          };
        }
      }
    },
  }),
  down: queryInterface => queryInterface.dropTable('pois')
}
