module.exports = {
  // eslint-disable-next-line
  up: async (queryInterface, Sequelize) => await queryInterface.createTable('areas', {
    'id': {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING,
      defaultValue: '',
      allowNull: false
    },
    geom: {
      type: Sequelize.GEOMETRY('POLYGON', 4326),
      defaultValue: '',
      allowNull: false
    },
    luas: {
      type: Sequelize.INTEGER(11),
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: false,
      defaultValue: Sequelize.fn('NOW')
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: false,
      defaultValue: Sequelize.fn('NOW')
    }
  },
    {
      hooks: {
        beforeSave: (instance, options) => {
          console.log('Saving geom: ' + instance.geom);
          if (instance.geom && !instance.geom.crs) {
            instance.geom.crs = {
              type: 'name',
              properties: {
                name: 'EPSG:4326'
              }
            };
          }
        }
      },
    }),
  down: queryInterface => queryInterface.dropTable('areas')
}
