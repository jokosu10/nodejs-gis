'use strict';
module.exports = function (sequelize, DataTypes) {
    var pois = sequelize.define('pois', {
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            unique: true
        },
        geom: {
            type: DataTypes.GEOMETRY('POINT', 4326),
            allowNull: false
        }
    }, {
            classMethods: {
                associate: function (models) {
                    // associations can be defined here
                }
            }
        });
    return pois;
};
