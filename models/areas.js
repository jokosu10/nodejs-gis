'use strict';
module.exports = function (sequelize, DataTypes) {
    var areas = sequelize.define('areas', {
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            unique: true
        },
        geom: {
            type: DataTypes.GEOMETRY('POLYGON', 4326),
            allowNull: false
        },
        luas: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
    }, {
            classMethods: {
                associate: function (models) {
                    // associations can be defined here
                }
            }
        });
    return areas;
};
