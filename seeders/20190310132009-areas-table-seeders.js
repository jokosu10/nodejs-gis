const config = require('../config/index');

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('areas', [{
    name: 'Area 1',
    geom: Sequelize.fn('ST_GeomFromText', 'POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))', 4326)
  }]),

  down: queryInterface => queryInterface.bulkDelete('areas', null, {})
}
