const config = require('../config/index');

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('pois', [{
    name: 'Berlin',
    geom: Sequelize.fn('ST_GeomFromText', 'POINT(52.458415 16.904740)', 4326)
  }]),

  down: queryInterface => queryInterface.bulkDelete('pois', null, {})
}
