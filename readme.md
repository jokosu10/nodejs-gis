### Cara menjalankan program
=============================

**Spesifikasi minimal software untuk menjalankan program**
* nodejs minimal versi **10.14.1**.
* npm minimal versi **6.4.1**.
* terminal / command prompt.
* postgresql minimal versi **11.0**.
* pgadmin minimal versi **4.0**.
* postman.
* git
* postgis ekstension
* browser (google chrome).

=============================

**Cara setting program**
1. Buka terminal / command prompt.
2. ketikkan perintah `git clone https://gitlab.com/jokosu10/nodejs-gis.git`
3. Masuk ke directory aplikasi.
4. Buka pgadmin aktifkan ekstensi `hstore`, `plpgsql`, `postgis`, `postgis_topology`
5. import file database `postgis-nsi`
6. setting konfigurasi aplikasi dan database di file `/config/config.json`
7. ketikan perintah `npm install` pada terminal untuk menginstall depedency
8. jalankan web server nodejs dengan perintah `node ./bin/www`


**Cara akses API di postman**

1. Buka postman anda.
2. import collection postman dari link berikut `https://www.getpostman.com/collections/506da7ab14009ead2b5e`
3. klik salah satu method.
4. silahkan masukkan sesuai path dan method yang ada.
5. disetiap method terdapat contoh response api yang berhasil.


**Cara menjalankan program di browser**

1. Untuk melihat hasil, buka browser ketikan alamat `http://localhost:3000/map/point`.
2. Untuk melihat hasil, buka browser ketikan alamat `http://localhost:3000/map/polygon`.

