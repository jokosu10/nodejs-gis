var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const Sequelize = require('sequelize');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var poisRouter = require('./routes/pois');
var areasRouter = require('./routes/areas');

const pg = require("pg");
const bodyParser = require('body-parser');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// configure app to use bodyParser
app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const dbCredentials = {
  host: 'localhost',
  port: 5432,
  user: 'joko',
  password: 'postgres',
  database: 'postgis_js'
}

/**
 * enable CORS
 */
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

//prefix all the routes with 'webgisapi'
let router = express.Router();
app.use('/webgisapi', router);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/pois/', poisRouter);
app.use('/api/areas/', areasRouter);


/**
 * errorneus response sending helper
 */
const sendErrorResponse = (res, msg) => {
  res.statusCode = 500;
  res.end(msg);
};

// get route to feature
router.route('/features').get((req, res) => {

  // setting client with conn details
  let client = new pg.Client(dbCredentials);

  client.connect(function(err) {
    if (err) {
      sendErrorResponse(res, 'Error Connection to the database: ' + err.message);
      return;
    }

    // interact with db
    client.query('SELECT id, ST_AsText(geom) as wkt from webgis;', (err, result) => {
      // close the connection when done
      client.end();

      if (err) {
        sendErrorResponse(res, 'Error reading features: ' + err.message);
        return;
      }

      if (result.rows.length === 0) {
        res.statusCode = 404;
      } else {
        res.statusCode = 200;
      }

      res.json(result.rows);
    });
  });
});

// post insert route to feature
router.route('/features').post((req, res) => {
  // setting client with conn details
  let client = new pg.Client(dbCredentials);

  client.connect(function (err) {
    if (err) {
      sendErrorResponse(res, 'Error Connection to the database: ' + err.message);
      return;
    }

    let wkt = req.body.wkt;

    //once connected we can now interact with a db
    client.query('INSERT INTO webgis (geom) values (ST_GeomFromText($1, 4326)) RETURNING id; ',[wkt], (err, result) => {
    //close the connection when done
    client.end();

    if (err) {
      sendErrorResponse(res, 'Error reading features: ' +
        err.message);
      return;
    }

    res.statusCode = 200;
    res.json({ id: result.rows[0].id, wkt: wkt });
  });
  });
});

// update route to feature by feature_id
router.route('/features/:feature_id').put((req, res) => {
  // init client with the appropriate conn details
  let client = new pg.Client(dbCredentials);

  client.connect((err) => {
    if (err) {
      sendErrorResponse(res, 'Error connecting to the database: ' + err.message);
      return;
    }

    // extract wkt off the request and id off the params
    let wkt = req.body.wkt;
    let id = req.params.feature_id;

    //once connected we can now interact with a db
    client.query('UPDATE webgis SET geom = ST_GeomFromText($1,4236) where id = $2;',[wkt, id],(err, result) => {
      // close connection when done
      client.end();

      if (err) {
        sendErrorResponse(res, 'Error reading features ' + err.message);
        return;
      }

      res.statusCode = 200;
      res.json({id:id, wkt:wkt});
    });
  });
});

// delete route to feature by feature_id
router.route('/features/:feature_id').delete((req, res) => {
  // init client with the appropriate conn details
  let client = new pg.Client(dbCredentials);

  client.connect((err) => {
    if (err) {
      sendErrorResponse(res, 'Error connecting to the database: ' + err.message);
      return;
    }

    // extract id off the request and id off the params
    let id = req.params.feature_id;

    //once connected we can now interact with a db
    client.query('DELETE FROM webgis where id = $1;',[id], (err, result) => {
      // close connection when done
      client.end();

      if (err) {
        sendErrorResponse(res, 'Error reading features ' + err.message);
        return;
      }

      res.statusCode = 200;
      res.json({ id: id});
    });
  });
});

// post insert route to feature buffers
router.route('/features/buffers').post((req, res) => {
  // setting client with conn details
  let client = new pg.Client(dbCredentials);

  client.connect(function (err) {
    if (err) {
      sendErrorResponse(res, 'Error Connection to the database: ' + err.message);
      return;
    }

    let wkt = req.body.wkt;
    let buffer = req.body.buffer;

    //once connected we can now interact with a db
    client.query('SELECT ST_AsText(ST_Buffer(ST_GeomFromText($1, 4326), $2)) as buffer;', [wkt, buffer], (err, result) => {
      //close the connection when done
      client.end();

      if (err) {
        sendErrorResponse(res, 'Error reading features: ' +
          err.message);
        return;
      }

      res.statusCode = 200;
      res.end(result.rows[0].buffer);
    });
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
