const conf = {};
conf.environment = 'development';
conf.sequelize = {};
conf.sequelize.username = 'joko';
conf.sequelize.password = 'postgres';
conf.sequelize.database = 'postgis_js';
conf.sequelize.host = '127.0.0.1';
conf.sequelize.dialect = 'postgres';
conf.sequelize.port = 5432;
conf.sequelize.define = {
    charset: 'utf8mb4',
    dialectOptions: {
        collate: 'utf8mb4_unicode_ci'
    }
}
conf.ROUND_SALT = 8;
module.exports = conf;
